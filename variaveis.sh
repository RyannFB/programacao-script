#!/bin/bash
#
#
#
echo "Formas de criar variáveis no bash: "


echo "Solicitar explicitamente que o usuário digite o valor de uma variável é feito usando o comando read, onde o script pausa e aguarda a entrada do usuário."

echo "Ex.: read "Escreva algo" {Variável que vai receber o valor escrito}"

echo "Receber uma variável como parâmetro de linha de comando envolve passar valores para o script quando ele é chamado na linha de comando. Esses valores podem ser acessados no script através de variáveis especiais, como $1, $2, etc., que representam o primeiro, segundo, e assim por diante, parâmetro."

echo 'Ex.: a1=$1'
