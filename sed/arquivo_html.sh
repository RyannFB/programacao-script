#!/bin/bash
#
#
#
#
#variáveis dos arquivos e data
#
arquivo_entrada="$1"
arquivo_saida="${arquivo_entrada}.html"
data_atual=$(date +"%d/%m/%Y às %H:%M horas")

#código html
{
    echo "<!DOCTYPE html>"
    echo "<html lang=\"pt-br\">"
    echo "  <head>"
    echo "    <title>$(basename "$arquivo_entrada")</title>"
    echo "    <meta charset=\"utf-8\">"
    echo "  </head>"
    echo "  <body>"

    # Processa o conteúdo do arquivo
    sed 's/amor/<a href="https:\/\/pt.wikipedia.org\/wiki\/Amor">amor<\/a>/g' "$arquivo_entrada"


    # Adiciona a data e hora de criação
    echo "  <br/> Criado em $data_atual"
    echo "  </body>"
    echo "</html>"
} > "$arquivo_saida"