#!/bin/bash

# Função para remover linhas em branco de um arquivo
rm_br() {
  local ar="$1"
  
  if [ -f "$ar" ]; then
    # Remove as linhas em branco e grava no mesmo arquivo
    sed -i '/^[[:space:]]*$/d' "$ar"
    echo "Linhas em branco removidas de $ar."
  else
    echo "O arquivo $ar não existe."
    exit 1
  fi
}

# Função para contar o número de linhas em branco em um arquivo
ct_br() {
  local ar="$1"

  if [ -f "$ar" ]; then
    # Conta o número de linhas em branco
    local ct=$(grep -c '^[[:space:]]*$' "$ar")
    echo "Número de linhas em branco em $ar: $ct"
  else
    echo "O arquivo $ar não existe."
    exit 1
  fi
}

# Função principal para processar as opções de linha de comando
processar_op() {
  local op="$1"
  local ar="$2"

  case "$op" in
    -r)
      rm_br "$ar"
      ;;
    -c)
      ct_br "$ar"
      ;;
    *)
      echo "Opção inválida. Use -r para remover linhas em branco ou -c para contar linhas em branco."
      exit 1
      ;;
  esac
}

# Verifica se o número de argumentos é correto
if [ "$#" -ne 2 ]; then
  echo "Uso: $0 [-r|-c] arquivo"
  exit 1
fi

# Processa as opções e o arquivo fornecido
processar_op "$1" "$2"
