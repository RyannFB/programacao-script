#!/bin/bash

function user() {
	local a1="$1"

	if id "$a1" &> /dev/null;then
		echo "Usuário existe"
		return 0
	else
		echo "O usuário não eixste"
		return 1
	fi
}

function logado() {
	local a1="$1"
	if user "$a1";then
		if who | grep -w "$a1" &> /dev/null; then
			echo "Está logado"
		else
			echo "Não está logado"
		fi
	fi
}



function home() {
	ls /home
}


function menu() {
  local es
  local us

  while true; do
    echo "Escolha uma opção:"
    echo "1. Verificar se o usuário existe"
    echo "2. Verificar se o usuário está logado"
    echo "3. Listar diretórios em /home"
    echo "4. Sair ou pressione a tecla d"

    read -r es

    case "$es" in
      1)
        read -p "Digite o nome do usuário: " us
        user "$us"
        ;;
      2)
        read -p "Digite o nome do usuário: " us
        logado "$us"
        ;;
      3)
        home
        ;;
      [Dd])
        echo "Saindo..."
        exit 0
        ;;
      *)
        echo "Opção inválida. Tente novamente."
        ;;
    esac
  done
}

# Executa o menu
menu
