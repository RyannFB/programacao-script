#!/bin/bash
#
#!/bin/bash

# Função para listar apenas diretórios
listar_diretorios() {
  local p1="$1"
  if [ -d "$p1" ]; then
    echo "Listando diretórios em $p1:"
    find "$p1" -maxdepth 1 -type d
  else
    echo "A pasta $p1 não existe."
    exit 1
  fi
}

# Função para listar apenas arquivos executáveis
listar_executaveis() {
  local p2="$1"
  if [ -d "$p2" ]; then
    echo "Listando arquivos executáveis em $p2:"
    find "$p2" -maxdepth 1 -type f -executable
  else
    echo "A pasta $p2 não existe."
    exit 1
  fi
}

# Função para listar apenas scripts shell
listar_scripts_shell() {
  local p3="$1"
  if [ -d "$p3" ]; then
    echo "Listando scripts shell em $p3:"
    find "$p3" -maxdepth 1 -type f -name "*.sh"
  else
    echo "A pasta $p3 não existe."
    exit 1
  fi
}

# Função principal para processar as opções de linha de comando
processar_opcoes() {
  local o1="$1"
  local p1="$2"

  case "$o1" in
    -a)
      listar_diretorios "$p1"
      ;;
    -b)
      listar_executaveis "$p1"
      ;;
    -c)
      listar_scripts_shell "$p1"
      ;;
    *)
      echo "Opção inválida. Use -a para listar diretórios, -b para executáveis, -c para scripts shell."
      exit 1
      ;;
  esac
}

# Verifica se o número de argumentos é correto
if [ "$#" -ne 2 ]; then
  echo "Uso: $0 [-a|-b|-c] pasta"
  exit 1
fi

# Processa as opções e o diretório fornecido
processar_opcoes "$1" "$2"
