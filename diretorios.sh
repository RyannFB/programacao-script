#!/bin/bash
#

a="/tmp"
b="/etc"
c="/bin"


echo "Diretório: $a"
echo "Quantidade de arquivos: $(ls -A "$a" | grep -vE '^\.$|^\.\.$' | wc -l)"
echo "Quantidade de diretórios: $(find "$a" -mindepth 1 -type d | wc -l)"
echo


echo "Diretório: $b"
echo "Quantidade de arquivos: $(ls -A "$b" | grep -vE '^\.$|^\.\.$' | wc -l)"
echo "Quantidade de diretórios: $(find "$b" -mindepth 1 -type d | wc -l)"
echo


echo "Diretório: $c"
echo "Quantidade de arquivos: $(ls -A "$c" | grep -vE '^\.$|^\.\.$' | wc -l)"
echo "Quantidade de diretórios: $(find "$c" -mindepth 1 -type d | wc -l)"

