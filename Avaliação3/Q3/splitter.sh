#!/bin/bash
#
#
arq=$1

con=0
cur=""

while IFS= read -r lin ;do

	if [ "$lin" == "----" ];then
		if [ -n "$cur" ];then
			echo "$cur" > "seção_${con}.txt"
			con=$(($con + 1))
			cur=""
		fi
	else
		cur="${cur}${lin}${IFS}"
	fi

done < "$arq"

if [ -n "$cur" ];then
	echo "$cur" > "seção_${con}.txt"
fi
