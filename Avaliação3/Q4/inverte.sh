#!/bin/bash

# Definir o caminho do arquivo
fil="/etc/group"

# Processar cada linha do arquivo
while IFS= read -r lin; do
    # Extrair os campos usando cut
    fi1=$(echo "$lin" | cut -d ':' -f 1)
    fi2=$(echo "$lin" | cut -d ':' -f 3) 
    fi3=$(echo "$lin" | cut -d ':' -f 4)  
    
    # Formatar o resultado
    for=":${fi2}:${fi3}:${fi1}:"
    
    # Exibir a linha formatada
    echo "$for"
done < "$fil"
