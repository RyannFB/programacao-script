#/bin/bash
#
#
input="Suprimir_data"
output="Suprimido"
exp="[^0-9 \t\n/]"

espaco="\s+"

sed -e "s/$exp//g" -e "s/$espaco/ /g" "$input"

echo $output
