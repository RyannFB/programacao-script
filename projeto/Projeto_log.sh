#!/bin/bash

# Verifica se foi passado um arquivo de configuração
if [ -z "$1" ]; then
  echo "Uso: $0 <arquivo_de_configuracao>"
  exit 1
fi

config_file="$1"

# Verifica se o arquivo de configuração existe
if [ ! -f "$config_file" ]; then
  echo "Arquivo de configuração não encontrado!"
  exit 1
fi

# Função para monitorar arquivos
monitor_log() {
  log_file=$(echo "$1" | tr -d '[:space:]')
  if [ -f "$log_file" ]; then
    tail -f "$log_file" | yad --text-info --title="Monitoramento de Logs: $log_file" --back=#000000 --fore=#FFFFFF --geometry=800x600 &
  else
    echo "Arquivo não encontrado: $log_file"
  fi
}

# Função para destacar múltiplos padrões
highlight_log() {
  log_file=$(echo "$1" | tr -d '[:space:]')
  highlight_patterns=$(echo "$2" | tr -d '[:space:]')
  if [ -f "$log_file" ]; then
    tail -f "$log_file" | grep --color=always -E "$highlight_patterns" | yad --text-info --title="Monitoramento e Destaque de Logs: $log_file" --back=#000000 --fore=#FFFFFF --geometry=800x600 &
  else
    echo "Arquivo não encontrado: $log_file"
  fi
}

# Função para destacar novas linhas com timestamp
highlight_new_lines() {
  log_file=$(echo "$1" | tr -d '[:space:]')
  if [ -f "$log_file" ]; then 
    tail -f "$log_file" | awk '{ print strftime("%Y-%m-%d %H:%M:%S "), $0; fflush(); }' | yad --text-info --title="Monitoramento com Destaque de Novas Linhas: $log_file" --back=#000000 --fore=#FFFFFF --geometry=800x600 &
  else
    echo "Arquivo não encontrado: $log_file"
  fi
}

# Função para exibir colunas específicas
filter_columns() {
  log_file=$(echo "$1" | tr -d '[:space:]')
  columns=$(echo "$2" | tr -d '[:space:]')
  if [ -f "$log_file" ]; then
    tail -f "$log_file" | awk -v cols="$columns" 'BEGIN{split(cols, a, ",")} {for(i in a) printf "%s ", $a[i]; print ""}' | yad --text-info --title="Monitoramento com Colunas Filtradas: $log_file" --back=#000000 --fore=#FFFFFF --geometry=800x600 &
  else
    echo "Arquivo não encontrado: $log_file"
  fi
}

# Função para ler e monitorar logs do arquivo de configuração
monitor_from_config() {
  while IFS= read -r line; do
    log_file=$(echo "$line" | tr -d '[:space:]') # Remove espaços em branco
    [ -z "$log_file" ] && continue # Pula linhas vazias
    monitor_log "$log_file"
  done < "$config_file"
}

# Menu interativo com yad
while true; do
  choice=$(yad --list --radiolist --title="Menu de Monitoramento de Logs" --column="Selecione" --column="Função" TRUE "Monitorar Log Local" FALSE "Destacar Padrão" FALSE "Destacar Novas Linhas" FALSE "Monitorar do Arquivo de Configuração" FALSE "Exibir Colunas Específicas" --height=400 --width=300 --separator=":")

  [ $? -eq 1 ] && exit 0

  option=$(echo "$choice" | awk -F ':' '{print $2}')

  case $option in
    "Monitorar Log Local")
      log_file=$(yad --file --title="Selecionar Arquivo de Log Local" --text="Selecione o arquivo de log local:")
      [ $? -eq 1 ] && continue
      monitor_log "$log_file"
      ;;
    "Destacar Padrão")
      log_file=$(yad --file --title="Selecionar Arquivo de Log" --text="Selecione o arquivo de log:")
      [ $? -eq 1 ] && continue
      patterns=$(yad --entry --title="Destacar Padrões" --text="Insira os padrões a serem destacados (separados por | para múltiplos):")
      [ $? -eq 1 ] && continue
      highlight_log "$log_file" "$patterns"
      ;;
    "Destacar Novas Linhas")
      log_file=$(yad --file --title="Selecionar Arquivo de Log" --text="Selecione o arquivo de log:")
      [ $? -eq 1 ] && continue
      highlight_new_lines "$log_file"
      ;;
    "Exibir Colunas Específicas")
      log_file=$(yad --file --title="Selecionar Arquivo de Log" --text="Selecione o arquivo de log:")
      [ $? -eq 1 ] && continue
      columns=$(yad --entry --title="Selecionar Colunas" --text="Insira os números das colunas a exibir (ex: 1,2,5):")
      [ $? -eq 1 ] && continue
      filter_columns "$log_file" "$columns"
      ;;
    "Monitorar do Arquivo de Configuração")
      monitor_from_config
      ;;
    *)
      echo "Seleção inválida."
      ;;
  esac
done

