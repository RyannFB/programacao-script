#!/bin/bash
#
#
#
#
function download() {
	./download.sh
}

function resume() {
	./linhas_log.sh
}

function listen_ips_random() {
	./ip_random.sh && cat ips_random.txt
}

function listen_ips() {
	./ip.sh && cat ips_unicos.txt
}

while true; do

	echo "Escolha qual opçãos:
	
	[1] Efetuar Download
	[2] Exibir Resumo do Arquivo
	[3] Listar IPs
	[4] Exibir IP aleatório
	[5] Sair"

	read -p "Digite qual opção você quer:" opcao

	case $opcao in
		1) download ;;
		2) resume ;;
		3) listen_ips ;;
		4) listen_ips_random ;;
		5)echo "Saindo..."; break ;;
	esac
done
